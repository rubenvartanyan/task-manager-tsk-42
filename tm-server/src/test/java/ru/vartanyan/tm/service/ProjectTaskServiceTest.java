package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.IProjectTaskService;
import ru.vartanyan.tm.api.service.ITaskService;
import ru.vartanyan.tm.dto.ProjectDTO;
import ru.vartanyan.tm.dto.TaskDTO;
import ru.vartanyan.tm.marker.DBCategory;
import ru.vartanyan.tm.dto.UserDTO;

import java.util.List;

public class ProjectTaskServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Test
    @Category(DBCategory.class)
    public void findAllTasksByProjectIdTest() throws Exception {
        final UserDTO user = new UserDTO();
        final String userId = user.getId();
        final ProjectDTO project = new ProjectDTO();
        final TaskDTO task1 = new TaskDTO();
        final TaskDTO task2 = new TaskDTO();
        final String projectId = project.getId();
        taskService.add(task1);
        taskService.add(task2);
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(projectId);
        task2.setProjectId(projectId);
        final long size = projectTaskService.findAllTaskByProjectId(projectId, userId).size();
        Assert.assertEquals(2, size);
    }

    @Test
    @Category(DBCategory.class)
    public void removeAllByProjectIdTest() throws Exception {
        final UserDTO user = new UserDTO();
        final String userId = user.getId();
        final ProjectDTO project = new ProjectDTO();
        final TaskDTO task1 = new TaskDTO();
        final TaskDTO task2 = new TaskDTO();
        final String projectId = project.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task1.setProjectId(projectId);
        task2.setProjectId(projectId);
        projectTaskService.removeProjectById(projectId, userId);
        final List<TaskDTO> taskList = projectTaskService.findAllTaskByProjectId(projectId, userId);
        Assert.assertEquals(0, taskList.size());
    }

    @Test
    @Category(DBCategory.class)
    public void bindTaskByProjectIdTest() throws Exception {
        final UserDTO user = new UserDTO();
        final String userId = user.getId();
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        final String projectId = project.getId();
        final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        final String taskId = task.getId();
        taskService.add(task);
        projectTaskService.bindTaskByProjectId(projectId, taskId, userId);
        Assert.assertEquals(projectId, task.getProjectId());
    }

    @Test
    @Category(DBCategory.class)
    public void unbindTaskFromProjectTest() throws Exception {
        final UserDTO user = new UserDTO();
        final String userId = user.getId();
        final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        final String projectId = project.getId();
        final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        final String taskId = task.getId();
        projectTaskService.bindTaskByProjectId(projectId, taskId, userId);
        projectTaskService.unbindTaskFromProject(projectId, taskId, userId);
        Assert.assertNull(task.getProjectId());
    }

}
