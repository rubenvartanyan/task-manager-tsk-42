package ru.vartanyan.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ISessionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.bootstrap.Bootstrap;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.marker.DBCategory;

import java.util.ArrayList;
import java.util.List;

public class SessionServiceTest {

    @NotNull
    private final ServiceLocator serviceLocator = new Bootstrap();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService, serviceLocator);

    @Test
    @Category(DBCategory.class)
    public void addAllSessionsTest() throws Exception {
        final List<SessionDTO> sessions = new ArrayList<>();
        final SessionDTO session1 = new SessionDTO();
        final SessionDTO session2 = new SessionDTO();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertNotNull(sessionService.findById(session1.getId()));
        Assert.assertNotNull(sessionService.findById(session2.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void addSessionTest() throws Exception {
        final SessionDTO session = new SessionDTO();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findById(session.getId()));
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void clearSessionsTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @SneakyThrows
    @Category(DBCategory.class)
    public void findAllSessions() {
        sessionService.clear();
        final List<SessionDTO> sessions = new ArrayList<>();
        final SessionDTO session1 = new SessionDTO();
        final SessionDTO session2 = new SessionDTO();
        sessions.add(session1);
        sessions.add(session2);
        try {
            sessionService.addAll(sessions);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findSessionByIdTest() throws Exception {
        final SessionDTO session = new SessionDTO();
        final String sessionId = session.getId();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findById(sessionId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeSessionByIdTest() throws Exception {
        final SessionDTO session1 = new SessionDTO();
        sessionService.add(session1);
        final String sessionId = session1.getId();
        sessionService.removeById(sessionId);
        Assert.assertNull(sessionService.findById(sessionId));
    }

}
