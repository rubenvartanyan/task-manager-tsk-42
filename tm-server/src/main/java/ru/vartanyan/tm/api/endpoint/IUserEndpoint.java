package ru.vartanyan.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    @Nullable
    UserDTO findUserByLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                            @WebParam (name = "session", partName = "session") @NotNull SessionDTO session) throws Exception;

    @WebMethod
    void removeUserByItsLogin(@WebParam (name = "login", partName = "login") @NotNull final String login,
                              @WebParam (name = "session", partName = "session") @NotNull SessionDTO session) throws Exception;

}
