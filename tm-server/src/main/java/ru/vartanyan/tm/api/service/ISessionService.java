package ru.vartanyan.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.system.WrongRoleException;

public interface ISessionService extends IService<SessionDTO> {

    @Nullable
    SessionDTO open(String login, String password)
            throws Exception;

    @SneakyThrows
    void validate(@Nullable SessionDTO session,
                  @Nullable Role role) throws Exception;

    @SneakyThrows
    void validate(@Nullable SessionDTO session) throws Exception;

    @SneakyThrows
    void validateAdmin(@Nullable SessionDTO session,
                       @Nullable Role role) throws Exception, WrongRoleException;

    @Nullable SessionDTO close(@Nullable SessionDTO session) throws Exception;

    boolean checkDataAccess(@NotNull final String login,
                            @NotNull final String password) throws Exception;

    SessionDTO sign(final SessionDTO session);

}
