package ru.vartanyan.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;

import java.sql.SQLException;
import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO findById(@NotNull final String id) throws Exception;

    void add(@NotNull final UserDTO user) throws Exception;

    @Nullable
    UserDTO findByLogin(@NotNull final String login) throws EmptyLoginException, SQLException;

    void remove(@NotNull final UserDTO user) throws Exception;

    void removeById(@NotNull final String id) throws Exception;

    void removeByLogin(@NotNull final String login) throws Exception;

    UserDTO create(@NotNull final String login,
                   @NotNull final String password) throws Exception;

    void create(@NotNull final String login,
                @NotNull final String password,
                @NotNull final String email) throws Exception;

    UserDTO create(@NotNull final String login,
                   @NotNull final String password,
                   @NotNull final Role role) throws Exception;

    void setPassword(@NotNull final String userId,
                     @NotNull final String password) throws Exception;

    void updateUser(@NotNull final String userId,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName) throws Exception;

    void unlockUserByLogin(@NotNull final String login) throws Exception;

    void unlockUserById(@NotNull final String id) throws Exception;

    void lockUserByLogin(@NotNull final String login) throws Exception;

    void lockUserById(@NotNull final String id) throws Exception;

    @Nullable
    List<UserDTO> findAll() throws Exception;

    void clear() throws Exception;

    void addAll(List<UserDTO> users) throws Exception;

}
