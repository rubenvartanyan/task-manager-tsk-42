package ru.vartanyan.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.IPropertyService;
import ru.vartanyan.tm.api.repository.ISessionRepository;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.api.service.ISessionService;
import ru.vartanyan.tm.api.service.ServiceLocator;
import ru.vartanyan.tm.dto.SessionDTO;
import ru.vartanyan.tm.dto.UserDTO;
import ru.vartanyan.tm.enumerated.Role;
import ru.vartanyan.tm.exception.empty.EmptyIdException;
import ru.vartanyan.tm.exception.empty.EmptyLoginException;
import ru.vartanyan.tm.exception.system.*;
import ru.vartanyan.tm.util.HashUtil;

import java.sql.SQLException;
import java.util.List;

public class SessionService extends AbstractService<SessionDTO> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull IConnectionService connectionService,
            @NotNull ServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @Override
    public @Nullable SessionDTO open(final String login,
                                     final String password)
            throws Exception {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final @Nullable UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        @Nullable final SessionDTO signSession = sign(session);
        if (signSession == null) return null;
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(signSession);
            sqlSession.commit();
            return signSession;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void validate(@Nullable SessionDTO session,
                         @Nullable Role role) throws Exception {
        if (role == null) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable SessionDTO session) throws AccessDeniedException, EmptyIdException, SQLException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        final String signatureSource = session.getSignature();
        final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        System.out.println(signatureSource);
        System.out.println(signatureTarget);
        System.out.println(signatureSource);
        System.out.println(signatureTarget);
        if (!check) throw new AccessDeniedException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        if (!(sessionRepository.findById(session.getId()) == null)) throw new AccessDeniedException();
    }

    @Override
    public void validateAdmin(@Nullable SessionDTO session,
                              @Nullable Role role) throws Exception, WrongRoleException {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        if ((session.getUserId()).isEmpty()) throw new AccessDeniedException();
        validate(session);
        final @Nullable UserDTO user = serviceLocator.getUserService().findById(session.getUserId());
        if (user == null) throw new AccessDeniedException();
        System.out.println(user.getRole());
        if (user.getRole() != Role.ADMIN) throw new WrongRoleException();
    }

    @Override
    public @Nullable SessionDTO close(@Nullable SessionDTO session) throws Exception {
        if (session == null) return null;
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeById(session.getId());
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean checkDataAccess(@NotNull String login,
                                   @NotNull String password)
            throws NoSuchUserException, EmptyLoginException, WrongPasswordException, SQLException {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        final UserDTO user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new NoSuchUserException();
        final String passwordHash = HashUtil.md5(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        boolean check = passwordHash.equals(user.getPasswordHash());
        if (!check) throw new WrongPasswordException();
        return (true);
    }

    @Override
    public SessionDTO sign(SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        final IPropertyService propertyService = serviceLocator.getPropertyService();
        final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void add(@Nullable final SessionDTO session) throws Exception {
        if (session == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void remove(@Nullable final SessionDTO session) throws Exception {
        if (session == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeById(session.getId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable final String id) throws EmptyIdException, SQLException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findById(id);
    }

    @Override
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() throws Exception {
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return sessionRepository.findAll();
    }

    public void clear() throws SQLException {
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    public void addAll(final List<SessionDTO> list) throws Exception {
        if (list == null) throw new NullObjectException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            list.forEach(sessionRepository::add);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean contains(@Nullable String id) throws EmptyIdException {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
        return (!(sessionRepository.findById(id) == null));
    }

}
