package ru.vartanyan.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.vartanyan.tm.api.IService;
import ru.vartanyan.tm.api.service.IConnectionService;
import ru.vartanyan.tm.dto.AbstractEntityDTO;

public abstract class AbstractService<E extends AbstractEntityDTO> implements IService<E> {

    @NotNull
    public final IConnectionService connectionService;

    public AbstractService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
