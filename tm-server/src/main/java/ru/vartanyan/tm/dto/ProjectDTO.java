package ru.vartanyan.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.api.entity.IWBS;

import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_project")
public class ProjectDTO extends AbstractBusinessEntityDTO implements IWBS {

    public ProjectDTO(@NotNull final String name,
                      @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", dateStarted=" + dateStarted +
                ", dateFinish=" + dateFinish +
                ", created=" + created +
                '}';
    }

}
