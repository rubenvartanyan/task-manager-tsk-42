package ru.vartanyan.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vartanyan.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "app_user")
public class User extends AbstractEntity{

    @Column
    @Nullable
    private String login;

    @Column(name = "password_hash")
    @Nullable private String passwordHash;

    @Column
    @Nullable private String email;

    @Column(name = "first_name")
    @Nullable private String firstName;

    @Column(name = "last_name")
    @Nullable private String lastName;

    @Column(name = "middle_name")
    @Nullable private String middleName;

    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role = Role.USER;

    @Column
    @NotNull private Boolean locked = false;

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

}
